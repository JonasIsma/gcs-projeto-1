var gcs = {};

(function ($, gcs) {

    $.fn.dataTable.ext.classes.sPageButton = 'btn-flat btn-small waves-effect';

    var appBaseUrl = '';

    gcs.setAppBaseUrl = function (baseUrl) {
        appBaseUrl = baseUrl;
    };

    gcs.getAppBaseUrl = function (method) {
        return appBaseUrl + method;
    };

    gcs.post = function (method, postData, callback, options) {
        var defaultOptions = $.extend({
            requestConfig: {
                type: "POST",
                dataType: "json",
                url: gcs.getAppBaseUrl(method),
                data: postData
            }
        }, options);

        var request = $.ajax(defaultOptions.requestConfig);

        request.success(function (response) {
            if (typeof callback === 'function') {
                callback(response);
            }
        });

        request.fail(function ($jqXHR) {
            if (typeof console !== 'undefined') {
                console.log($jqXHR);
                console.log('Ocorreu um erro ao executar a chamada dyn.ajax.');
            }
        });
    };

    gcs.upload = function (method, postData, callback, options) {
        var defaultOptions = $.extend({
            requestConfig: {
                type: "POST",
                dataType: "text",
                url: gcs.getAppBaseUrl(method),
                data: postData,
                cache: false,
                contentType: false,
                processData: false,
            }
        }, options);

        var request = $.ajax(defaultOptions.requestConfig);

        request.success(function (response) {
            if (typeof callback === 'function') {
                callback(response);
            }
        });

        request.fail(function ($jqXHR) {
            if (typeof console !== 'undefined') {
                console.log($jqXHR);
                console.log('Ocorreu um erro ao executar a chamada dyn.ajax.');
            }
        });
    };

    gcs.html = {};

    gcs.html.setFormFieldsMasks = function (options) {
        var defaultOptions = $.extend({
            clearIfNotMatch: true,
            moneyMaskPrefix: '',
            moneyMaskAllowNegative: false,
            moneyMaskThousandsSeparator: '.',
            moneyMaskDecimalSeparator: ',',
            moneyMaskAffixesStay: false,
            moneyMaskAllowZero: true,
            calendarAutoClose: true,
            calendarShowOnFocus: false,
            calendarWeeks: true,
            calendarOrientation: 'auto'
        }, options);

        function foneMaskBehavior(val)
        {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-0000#';
        }

        var foneMaskOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(foneMaskBehavior.apply({}, arguments), options);
            },
            clearIfNotMatch: defaultOptions.clearIfNotMatch
        };

        function dataDataHoraMaskBehavior(val)
        {
            return val.replace(/\D/g, '').length > 8 ? '00/00/0000 00:00:00' : '00/00/0000#';
        }

        var dataDataHoraMaskOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(dataDataHoraMaskBehavior.apply({}, arguments), options);
            },
            clearIfNotMatch: defaultOptions.clearIfNotMatch
        };

        $('input.campo-cep').mask('00.000-000', defaultOptions);
        $('input.campo-telefone').mask(foneMaskBehavior, foneMaskOptions);
        $('input.campo-data-data-hora').mask(dataDataHoraMaskBehavior, dataDataHoraMaskOptions, {maxlength: false});
        $('input.campo-numero').mask('#');
        $('input.campo-valor').maskMoney({
            prefix: defaultOptions.moneyMaskPrefix,
            allowNegative: defaultOptions.moneyMaskAllowNegative,
            thousands: defaultOptions.moneyMaskThousandsSeparator,
            decimal: defaultOptions.moneyMaskDecimalSeparator,
            affixesStay: defaultOptions.moneyMaskAffixesStay,
            allowZero: defaultOptions.moneyMaskAllowZero
        });
        $('input.campo-data-hora').mask('00/00/0000 00:00:00', defaultOptions);
        $('input.campo-somente-data').mask('00/00/0000', defaultOptions);
        $('input.campo-hora').mask('00:00', defaultOptions);
        $('input.campo-mesano').mask('00/0000', defaultOptions);
        $('input.campo-dataini-datafim').mask('00/0000 - 00/0000', defaultOptions);
        $('input.campo-diames').mask('00/00', defaultOptions);
        $('input.campo-cnpj').mask('00.000.000/0000-00', defaultOptions);
        $('input.campo-cpf').mask('000.000.000-00', defaultOptions);

        var defaultOptionsDate = {
            zIndex: 1030,
            autoHide: true,
            autoPick: false,
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
            daysMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        };

        var $inputCampoData = $('input.campo-data:not(:disabled)');

        $inputCampoData.each(function () {
            var $input = $(this);

            if ($input.attr('readonly')) {
                return;
            }

            $input.mask('00/00/0000');
            $input.datepicker(defaultOptionsDate);

            $(this).on('blur change', function () {
                if ($(this).val()) {
                    $(this).addClass('has-value');

                    return;
                }

                $(this).removeClass('has-value');
            });

            $(this).parent().find('.calendar-icon').on('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                $input.datepicker('show');
            });

            $(this).parent().find('.calendar-clear').on('click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                $input.val('');
                $input.removeClass('has-value');
                $input.focus();
            });

            if ($(this).val()) {
                $input.addClass('has-value');

                return;
            }

            $input.removeClass('has-value');
        });

        $(document).on("blur", "input.campo-data", function () {
            var $obj = $(this);
            var val = $(this).val();

            switch (val.length) {
                case 2:
                    val = val + '/';
                case 3:
                    var mes = moment().format('MM');
                    var data = val + mes;

                    if (moment(data, 'DD/MM').isValid()) {
                        $obj.val(data + '/' + moment().format('YYYY'))
                    } else {
                        $obj.val('');
                    }

                    break;
                case 6:
                    val = val.substring(0, 5);
                case 5:
                    if (moment(val, 'DD/MM').isValid()) {
                        $obj.val(val + '/' + moment().format('YYYY'))
                    } else {
                        $obj.val('');
                    }
                    break;
                case 10:
                    if (!moment(val, 'DD/MM/YYYY').isValid()) {
                        $obj.val('');
                    }
                    break;
                default:
                    $obj.val('');
            }
        });
    };

    $(document).on("click", ".modal-close", function (e) {
        e.preventDefault();

        var $modal = $(this).closest('.modal');
        $modal.remove();
    });

    $(document).ajaxStart(function () {
        var overlay = $('<div id="loadingOverlay"><div class="overlayContent"><img src="' + gcs.getAppBaseUrl(
            'images/ajax-loader.gif') + '" alt="Aguarde..." /></div></div>');

        var body = $('body');
        body.append(overlay);

        body.loading({
            overlay: overlay
        });
    });

    $(document).ajaxStop(function () {
        $('body').loading('stop');
    });

    gcs.aplicarMascaras = function () {
        $(".campo-datahora").mask("99/99/9999 99:99");
        $(".campo-data").mask("00/00/0000");
        $(".campo-ano").mask("9999");
    };

    $('.dropdown-trigger').dropdown({
        coverTrigger: false,
        alignment: 'right',
        constrainWidth: false
    });

    $(document).on('ready', function () {
        gcs.aplicarMascaras();
        $('select').formSelect();
    });
}(jQuery, gcs));