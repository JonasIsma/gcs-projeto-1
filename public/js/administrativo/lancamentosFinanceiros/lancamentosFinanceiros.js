(function ($, gcs) {
    let configDataTable = {
        "language": {
            "paginate": {
                "next": "Próxima",
                "previous": "Anterior"
            },
            "info": "Exibindo registro _START_ ao _END_ de _TOTAL_",
            "infoFiltered": " (Filtrado de _MAX_ registros)",
            "emptyTable": "Nenhum registro encontrado.",
            "infoEmpty": "Sem registros para exibir",
            "zeroRecords": "Nenhum registro encontrado.",
            "search": "Pesquisar:"
        },
        "dom": '<"dt-top"<"clearfix">f><"table-responsive altura-fixa"t><"dt-bottom"ip<"clearfix">>',
        "pageLength": 10,
        "lengthChange": false,
        "filter": true,
    };

    $('#table-lancamentosFinanceiros').dataTable(configDataTable);

    $(document).on('ready', function() {
        gcs.html.setFormFieldsMasks();
    });

}(jQuery, gcs));