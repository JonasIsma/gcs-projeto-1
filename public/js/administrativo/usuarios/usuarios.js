(function ($, gcs) {
    let configDataTable = {
        "language": {
            "paginate": {
                "next": "Próxima",
                "previous": "Anterior"
            },
            "info": "Exibindo registro _START_ ao _END_ de _TOTAL_",
            "infoFiltered": " (Filtrado de _MAX_ registros)",
            "emptyTable": "Nenhum registro encontrado.",
            "infoEmpty": "Sem registros para exibir",
            "zeroRecords": "Nenhum registro encontrado.",
            "search": "Pesquisar:"
        },
        "dom": '<"dt-top"<"clearfix">f><"table-responsive altura-fixa"t><"dt-bottom"ip<"clearfix">>',
        "pageLength": 10,
        "lengthChange": false,
        "filter": true,
    };

    $('#table-usuarios').dataTable(configDataTable);

    $('#table-setores').dataTable(configDataTable);

    $("input[name=gerarToken]").on('change', function () {
        let $senha = $("input[name=senha]");
        let $confirmasenha = $("input[name=confirmasenha]");
        let $dangerSenha = $("small#danger-senha");
        let $dangerConfirmaSenha = $("small#danger-confirmasenha");

        if ($(this).val() === '1') {
            $senha.val('');
            $senha.prop('disabled', true);
            $dangerSenha.addClass('hide');

            $confirmasenha.val('');
            $confirmasenha.prop('disabled', true);
            $dangerConfirmaSenha.addClass('hide');

            return;
        }

        $senha.prop('disabled', false);
        $dangerSenha.removeClass('hide');
        $confirmasenha.prop('disabled', false);
        $dangerConfirmaSenha.removeClass('hide');
    });

    $('select[name=modulo]').on('change', function () {
        let $permissao = $("select[name=permissao]");

        $permissao.empty();

        if ($(this).val()) {
            let postData = {
                modulo: $(this).val()
            };

            gcs.post('administrativo/usuarios/getPermissoesByModulo', postData, function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    $permissao.append(
                        '<option value="' + response.data[i]['id'] + '">' + response.data[i]['descricao'] + '</option>');
                }

                $permissao.formSelect();
            });
        }


    }).trigger('change');

    $('#btn-inclui-permissao').on('click', function (e) {
        e.preventDefault();

        let $modulo = $('select[name=modulo]');
        let $permissao = $("select[name=permissao]");

        if ($modulo.val() && $permissao.val()) {
            let $tbody = $("#usuario-permissoes").find('tbody');
            let $option = $permissao.find("option:selected");

            if ($tbody.find('input[value=' + $permissao.val() + ']').length > 0) {
                alertify.error('Permissão já cadastrada.');

                return;
            }

            let $td1 = $("<td></td>").html($modulo.find('option:selected').text());
            let $td2 = $("<td></td>").html($option.text());
            $td2.append('<input type="hidden" name="permissoes[]" value="' + $permissao.val() + '"/>');
            let $td3 = $(
                "<td class='right-align'><a href='#' class='remover-permissao' title='Clique para remover'><i class='fa fa-trash'></i></a></td>");

            let $tr = $("<tr></tr>");
            $tr.append($td1);
            $tr.append($td2);
            $tr.append($td3);

            $tbody.append($tr);
        }
    });

    $('#btn-inclui-setor').on('click', function (e) {
        e.preventDefault();

        let $setor = $('select[name=setor]');

        if ($setor.val()) {
            let $tbody = $("#usuario-setores").find('tbody');

            if ($tbody.find('input[value=' + $setor.val() + ']').length > 0) {
                alertify.error('Setor já cadastrado.');

                return;
            }

            let $td1 = $("<td></td>").html($setor.find('option:selected').text());
            $td1.append('<input type="hidden" name="setores[]" value="' + $setor.val() + '"/>');
            let $td2 = $(
                "<td class='right-align'><a href='#' class='remover-setor' title='Clique para remover'><i class='fa fa-trash'></i></a></td>");

            let $tr = $("<tr></tr>");
            $tr.append($td1);
            $tr.append($td2);

            $tbody.append($tr);
        }
    });

    $(document).on('click', '.remover-permissao', function (e) {
        e.preventDefault();

        let $caller = $(this);

        alertify.confirm('Deseja remover a permissão?', function () {
            $caller.closest('tr').remove();
        });
    });

    $(document).on('click', '.remover-setor', function (e) {
        e.preventDefault();

        let $caller = $(this);

        alertify.confirm('Deseja remover o vinculo com o Setor?', function () {
            $caller.closest('tr').remove();
        });
    });

    $("input[name=alterarSenha]").on('change', function (e) {
        e.preventDefault();

        var $div = $("#div-senha-options");

        $("input[name=senha]").val('');
        $("input[name=confirmasenha]").val('');

        $('#gerarToken1').prop("checked", true);

        if ($(this).is(':checked')) {
            $div.removeClass('hide');
            $div.find('input[type=radio]').each(function () {
                $(this).prop('disabled', false);
            });

            return;
        }

        $div.addClass('hide');
        $div.find('input').each(function () {
            $(this).prop('disabled', true);
        });
    });

    $('.remover-importacao').on('click', function (e) {
        e.preventDefault();

        var $caller = $(this);

        alertify.confirm('Deseja remover a instrução de importação?', function () {
            $caller.closest('tr').remove();
        });
    });

}(jQuery, gcs));