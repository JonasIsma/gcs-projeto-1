<?php


use Phinx\Migration\AbstractMigration;

class Log extends AbstractMigration
{
    public function up()
    {
        $this->table('log')
            ->addColumn('datahora', 'datetime')
            ->addColumn('idusuario', 'integer')
            ->addColumn('tipo', 'string', array('limit' => 3))
            ->addColumn('mensagem', 'text')
            ->addColumn('programa', 'string', array('limit' => 40))
            ->addColumn('idreferencia', 'integer', array('null' => true))
            ->addColumn('observacoes', 'text', array('null' => true))
            ->addForeignKey('idusuario', 'usuarios', 'id', array('delete' => 'CASCADE'))
            ->save();
    }

    public function down()
    {
        $this->dropTable('log');
    }
}
