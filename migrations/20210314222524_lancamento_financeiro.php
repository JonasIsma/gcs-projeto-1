<?php


use Phinx\Migration\AbstractMigration;

class LancamentoFinanceiro extends AbstractMigration
{

    public function up()
    {
        $this->table('lancamentosfinanceiros')
            ->addColumn('descricao', 'string', array('limit' => 256, 'null' => false))
            ->addColumn('dataregistro', 'timestamp', array('null' => false))
            ->addColumn('datavencimento', 'timestamp', array('null' => false))
            ->addColumn('status', 'integer', array('null' => false))
            ->addColumn('observacao', 'string', array('limit' => 256, 'null' => true))
            ->addColumn('valor', 'float', array('null' => false))
            ->addColumn('datapagamento', 'timestamp', array('null' => true))
            ->addColumn('tipo', 'integer', array('null' => false))
            ->save();
    }

    public function down()
    {
        $this->dropTable('lancamentosfinanceiros');
    }
}
