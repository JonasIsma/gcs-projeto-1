<?php
require_once 'vendor/autoload.php';

$dotEnv = new \Dotenv\Dotenv(__DIR__);
$dotEnv->load();

$dotEnv->required('environment')->allowedValues(array('development', 'testing', 'production'));
$dotEnv->required('dbuser');
$dotEnv->required('dbpass');
$dotEnv->required('dbname');
$dotEnv->required('dbhost');
$dotEnv->required('mailgun_apikey');
$dotEnv->required('mailgun_domain');
$dotEnv->required('tagfile');

if (getenv('environment') !== 'production') {
    $dotEnv->required('email');
}

//Define timezone ao invés de utilizar a do sistema operacional
date_default_timezone_set('America/Sao_Paulo');