<?php

use toshyro\gcs\LogWriter;
use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\QueryCriteria;

class Logs extends BaseController
{
    public function index()
    {
        /** @var \toshyro\gcs\repository\LogRepository $repository */
        $repository = \toshyro\gcs\repository\RepositoryFactory::make('log');

        if ($this->input->post('quantidade') != '') {
            $repository->setMaxRows((int)$this->input->post('quantidade'));
        }

        $criteria = array();

        if ($this->input->post('idusuario') != '') {
            $criteria[] = new QueryCriteria('x.idusuario', $this->input->post('idusuario'));
        }

        if ($this->input->post('tipo') != '') {
            $criteria[] = new QueryCriteria('x.tipo', $this->input->post('tipo'));
        }

        $viewData['registrosLog'] = $repository->findBy(
            $criteria,
            array(
                'x.dataHora' => 'desc',
            )
        );

        $viewData['usuarios'] = $this->usuarioRepository->findAll(
            array('nome' => 'asc')
        );

        $viewData['tiposAlteracoes'] = array();

        $item            = new stdClass();
        $item->id        = LogWriter::TIPO_LOG_INSERCAO;
        $item->descricao = 'Inserção';

        $viewData['tiposAlteracoes'][] = $item;

        $item            = new stdClass();
        $item->id        = LogWriter::TIPO_LOG_ALTERACAO;
        $item->descricao = 'Alteração';

        $viewData['tiposAlteracoes'][] = $item;

        $item            = new stdClass();
        $item->id        = LogWriter::TIPO_LOG_EXCLUSAO;
        $item->descricao = 'Exclusão';

        $viewData['tiposAlteracoes'][] = $item;

        $this->twigDisplay('administrativo/logs/log_index', $viewData);
    }
}