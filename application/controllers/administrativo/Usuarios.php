<?php

use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;
use toshyro\gcs\service\MailgunService;

class Usuarios extends BaseController
{
    public function index()
    {
        $usuarios = RepositoryFactory::make('usuarios')
                                     ->findAll(array('ativo, nome' => 'ASC'));

        $this->twigDisplay('administrativo/usuarios/usuarios_list', array('usuarios' => $usuarios));
    }

    public function add()
    {
        $viewData['modulos'] = RepositoryFactory::make('modulos')->findAll(array('descricao' => 'ASC'));

        if ($this->input->post()) {
            try {

                $data = $this->input->post();

                if($data['gerarToken'] == '1'){
                    $data['tokenacesso'] = bin2hex(openssl_random_pseudo_bytes(16));
                }

                $id = RepositoryFactory::make('usuarios')->insert($data);

                if($data['gerarToken'] == '1'){
//                    $this->enviarUsuarioEmail($id);
                }

                redirect('administrativo/usuarios/edit/' . $id);
            } catch (Exception $e) {
                $viewData['errorMessage'] = $e->getMessage();
                $this->twigError('administrativo/usuarios/usuarios_add', $viewData);

                return;
            }
        }

        $this->twigDisplay('administrativo/usuarios/usuarios_add', $viewData);
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            RepositoryFactory::make('usuarios')->update($this->input->post());

            $viewData['serverSuccess'] = 'Dados atualizados com sucesso.';
        }

        $viewData['modulos'] = RepositoryFactory::make('modulos')->findAll(array('descricao' => 'ASC'));

        $viewData['usuario'] = RepositoryFactory::make('usuarios')->findByID($id);
        $viewData['permissoes'] = RepositoryFactory::make('usuariospermissoes')->getPermissoesByUsuario($id);

        $this->twigDisplay('administrativo/usuarios/usuarios_edit', $viewData);
    }

    public function ativar($id)
    {
        RepositoryFactory::make('usuarios')->ativo($id, 1);

        redirect('administrativo/usuarios');
    }

    public function inativar($id)
    {
        RepositoryFactory::make('usuarios')->ativo($id, 0);

        redirect('administrativo/usuarios');
    }

    public function getPermissoesByModulo()
    {
        if ($this->input->is_ajax_request() === false) {
            exit('No direct script access allowed');
        }

        $permissoes = RepositoryFactory::make('permissoes')
                                       ->findBy(array(
                                           new QueryCriteria('idModulo', $this->input->post('modulo')),
                                       ));

        $this->ajaxOutput($permissoes);
    }

    private function enviarUsuarioEmail($idusuario){

        $usuario = RepositoryFactory::make('usuarios')->findByID($idusuario);

        $usuario->url = base_url("sistema/login/newPassword/{$usuario->tokenacesso}");

        $html = $this->twig->render('sistema/login/login_novo_usuario', array('usuario' => $usuario));

        MailgunService::getInstance()
                      ->setTo($usuario->email)
                      ->setSubject('Novo usuário')
                      ->setMessage($html)
                      ->send();
    }
}