<?php

use toshyro\gcs\repository\RepositoryFactory;

class Dashboard extends \toshyro\gcs\mvc\BaseController
{
    public function index()
    {
        $modulosComPermissao = RepositoryFactory::make('modulos')
                                                ->modulosComPermissao(getIdUsuarioAutenticado());

        if (count($modulosComPermissao) === 1) {
            redirect($modulosComPermissao[0]->urisegment);
        }

        $modulos = array();

        foreach ($modulosComPermissao as $moduloComPermissao) {
            $modulos[$moduloComPermissao->id] = $moduloComPermissao;
        }

        $this->twigDisplay('sistema/dashboard/dashboard', array('modulos' => $modulos));
    }
}