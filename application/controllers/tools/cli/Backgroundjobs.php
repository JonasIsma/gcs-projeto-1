<?php

use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;
use toshyro\gcs\service\MailgunService;
use toshyro\helpers\StatusRegistroHelper;

class Backgroundjobs extends BaseController
{
    public function index(){
        self::validationCli();

        self::notificacoesDiarias();

        self::registrosVencidos();
    }

    private function notificacoesDiarias(){
        self::validationCli();

        $ci = &get_instance();
        $today = new DateTime();

        $notificacoesDiarias = RepositoryFactory::make('notificacoes')->findBy(
            array(
                new QueryCriteria('datavalidade', $today->format('Y-m-d')),
            )
        );

        foreach ($notificacoesDiarias as $notificacao){
            $registro = RepositoryFactory::make('registros')->findByID($notificacao->idregistro);
            $registro->datavalidade = formatForView($registro->datavalidade);

            if($registro->status){
                continue;
            }

            if($registro->idpai){
                $data['pai'] = RepositoryFactory::make('registros')->findByID($registro->idpai);
            }else{
                $data['pai'] = $registro;
                $data['etapas'] = RepositoryFactory::make('registros')->findBy(
                    array( new QueryCriteria('idpai', $registro->id) ),
                    array('sequencia' => 'ASC', 'datavalidade' => 'ASC')
                );

                foreach ($data['etapas'] as &$etapa){
                    $etapa->status = StatusRegistroHelper::getInstance()->getDisplayStatus($etapa->id);
                }
            }

            $data['arquivos'] = RepositoryFactory::make('arquivos')->findBy(
                array(
                    new QueryCriteria('idregistro', $registro->id)
                )
            );

            foreach ($data['arquivos'] as &$arquivo){
                $arquivo->url = base_url($arquivo->localupload.$arquivo->descricao);
                $arquivo->status = $arquivo->status ? 'Público' : 'Privado';
            }

            if(!empty($registro->setor)){

                $usuariosSetores = RepositoryFactory::make('usuarios')->findUsersBySectors(array(
                    new QueryCriteria('setor.descricao',array_values(explode(';', $registro->setor)),'likeIn')
                ));

                foreach ($usuariosSetores as $usuarioSetor){
                    $pos = strpos($registro->envolvidos, $usuarioSetor->email);
                    if ($pos === false) {
                        if(strlen($registro->envolvidos) < 1){
                            $registro->envolvidos = $usuarioSetor->email;
                        }else{
                            $registro->envolvidos .=  ';' . $usuarioSetor->email;
                        }

                    }
                }
            }

            $data['registro'] = $registro;
            $data['notificacao'] = $notificacao;

            $html = $ci->twig->render('email/email_padrao_notificacao', $data);

            MailgunService::getInstance()
                ->setTo($registro->envolvidos)
                ->setSubject('Notificação de lembrete de registro/etapa')
                ->setMessage($html)
                ->send();
        }
    }

    public function registrosVencidos(){
        self::validationCli();

        $ci = &get_instance();
        $today = new DateTime();
        $parametro = RepositoryFactory::make('parametros')->findByID('1');

        $registrosVencidos = RepositoryFactory::make('registros')->findBy(
            array(
                new QueryCriteria('datavalidade', $today->format('Y-m-d'), '<'),
                new QueryCriteria('status', 0),
            )
        );

        foreach ($registrosVencidos as $registro){
            $dataRegistro = new DateTime($registro->datavalidade);

            $interval = $dataRegistro->diff($today);

            $diasEntre = $interval->d;

            if(!($diasEntre % $parametro->relembrarvencidos == 0)){
                continue;
            }

            $registro->datavalidade = formatForView($registro->datavalidade);
            $etapa = null;
            if($registro->idpai){
                $etapa = $registro;

                $registro = RepositoryFactory::make('registros')->findByID($etapa->idpai);
            }

            if(!empty($registro->setor)){

                $usuariosSetores = RepositoryFactory::make('usuarios')->findUsersBySectors(array(
                    new QueryCriteria('setor.descricao',array_values(explode(';', $registro->setor)),'likeIn')
                ));

                foreach ($usuariosSetores as $usuarioSetor){
                    $pos = strpos($registro->envolvidos, $usuarioSetor->email);
                    if ($pos === false) {
                        if(strlen($registro->envolvidos) < 1){
                            $registro->envolvidos = $usuarioSetor->email;
                        }else{
                            $registro->envolvidos .=  ';' . $usuarioSetor->email;
                        }

                    }
                }
            }

            $data['registro'] = $registro;
            $data['etapa'] = $etapa;

            $html = $ci->twig->render('email/email_padrao_registros_vencidos', $data);

            MailgunService::getInstance()
                ->setTo($registro->envolvidos)
                ->setSubject('Notificação de lembrete de registro/etapa')
                ->setMessage($html)
                ->send();
        }

    }

    //valdiador

    private function validationCli(){
        if (!is_cli()) {
            log_message('ERROR',
                'BACKGROUND JOBS - O sistema de background jobs recebeu uma tentativa de utilização fora do cli.');

            die;
        }
    }
}