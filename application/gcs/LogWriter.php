<?php

namespace toshyro\gcs;

use toshyro\gcs\repository\RepositoryFactory;

class LogWriter
{
    const TIPO_LOG_ALTERACAO = 'ALT';
    const TIPO_LOG_INSERCAO  = 'INS';
    const TIPO_LOG_EXCLUSAO  = 'EXC';

    private $logData;

    protected function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $dataHora = new \DateTime();

        $this->logData = array(
            'datahora'  => $dataHora->format('Y-m-d H:i:s'),
            'idusuario' => getIdUsuarioAutenticado(),
        );
    }

    /**
     * @param $tipo
     *
     * @return LogWriter
     */
    public function setTipo($tipo)
    {
        $this->logData['tipo'] = $tipo;

        return $this;
    }

    /**
     * @param $mensagem
     *
     * @return LogWriter
     */
    public function setMensagem($mensagem)
    {
        $this->logData['mensagem'] = $mensagem;

        return $this;
    }

    /**
     * @param $programa
     *
     * @return LogWriter
     */
    public function setPrograma($programa)
    {
        $this->logData['programa'] = $programa;

        return $this;
    }

    /**
     * @param $idReferencia
     *
     * @return LogWriter
     */
    public function setIdReferencia($idReferencia)
    {
        $this->logData['idreferencia'] = $idReferencia;

        return $this;
    }

    /**
     * @param $observacoes
     *
     * @return LogWriter
     */
    public function setObservacoes($observacoes)
    {
        $this->logData['observacoes'] = $observacoes;

        return $this;
    }

    /**
     *
     * @return LogWriter
     */
    public function setEnviado()
    {
        $this->logData['enviado'] = true;

        return $this;
    }

    public function write()
    {
        $this->logData['idusuario'] = $this->logData['idusuario'] !== null ? $this->logData['idusuario'] : $this->logData['idreferencia'];

        RepositoryFactory::make('log')
                         ->insert($this->logData);

        $this->reset($this->logData);
    }

    /**
     * @return LogWriter
     */
    public static function getInstance()
    {
        return new self;
    }
}