<?php

namespace toshyro\gcs;

interface IObservable
{
    public function attach(IObserver $observer);

    public function detach(IObserver $observer);

    public function notify();
}