<?php

namespace toshyro\gcs\mvc;

use toshyro\gcs\repository\RepositoryFactory;

abstract class BaseController extends \CI_Controller
{
    protected $usuarioRepository;
    protected $edicaoRepository;
    protected $orientadorRepository;

    public function __construct()
    {
        parent::__construct();
        $this->usuarioRepository    = RepositoryFactory::make('usuarios');
    }

    protected function ajaxOutput($viewData, $viewErrors = null)
    {
        $response = array();

        $response['data'] = $viewData;

        if ($viewErrors) {
            unset($response['data']);
            $response['errors'] = $viewErrors;
        }

        $this->output->set_output(json_encode($response));
    }

    protected function twigDisplay($viewName, $viewData = array())
    {
        $viewData = array_merge($viewData, getUsuarioLogadoViewData());

        $this->twig->display($viewName, $viewData);
    }

    protected function twigSuccess($viewName, $viewData = array())
    {
        $defaultMessage = 'Informações salvas com sucesso.';

        if (array_key_exists('successMessage', $viewData)) {
            $defaultMessage = $viewData['successMessage'];
        }

        $viewData['serverSuccess'] = $defaultMessage;

        $this->twigDisplay($viewName, $viewData);
    }

    protected function twigError($viewName, $viewData = array())
    {
        $defaultMessage = 'Ocorreram erros ao processar a solicitação.';

        if (array_key_exists('errorMessage', $viewData)) {
            $defaultMessage = $viewData['errorMessage'];
        }

        $viewData['serverError'] = $defaultMessage;

        $this->twigDisplay($viewName, $viewData);
    }
}