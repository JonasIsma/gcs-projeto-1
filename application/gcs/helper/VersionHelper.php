<?php

namespace toshyro\gcs\helper;

class VersionHelper
{
    public function getLatestVersionIssues()
    {
        $curl = curl_init();

        $post   = array(
            'jql' => sprintf('project = 11310 AND fixVersion = %s', $this->getCurrentVersion()),
        );
        $post   = json_encode($post);
        $header = array(
            'Content-Type: application/json',
            sprintf('Content-Length: %s', strlen($post)),
        );

        curl_setopt_array($curl, array(
            CURLOPT_URL            => 'https://toshyro.atlassian.net/rest/api/2/search',
            CURLOPT_USERPWD        => sprintf('%s:%s', getenv('jirausername'), getenv('jirapassword')),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => $post,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_HTTPHEADER     => $header,
        ));

        $return = json_decode(curl_exec($curl));
        curl_close($curl);

        $issues = array();

        foreach ($return->issues as $issue) {
            $issues[] = array(
                'summary'     => $issue->fields->summary,
                'description' => $issue->fields->customfield_10600,
            );
        }

        $return = array(
            'issues'      => $issues,
            'version'     => $this->getCurrentVersion(),
            'releaseDate' => \DateTime::createFromFormat('Y-m-d',
                $return->issues[0]->fields->fixVersions[0]->releaseDate),
        );

        return $return;
    }

    public function getIssuesGroupByVersion()
    {
        $curl = curl_init();

        $post   = array(
            'jql' => sprintf('project = 11310'),
        );
        $post   = json_encode($post);
        $header = array(
            'Content-Type: application/json',
            sprintf('Content-Length: %s', strlen($post)),
        );

        curl_setopt_array($curl, array(
            CURLOPT_URL            => 'https://toshyro.atlassian.net/rest/api/2/search',
            CURLOPT_USERPWD        => sprintf('%s:%s', getenv('jirausername'), getenv('jirapassword')),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => $post,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_HTTPHEADER     => $header,
        ));

        $return = json_decode(curl_exec($curl));
        curl_close($curl);

        $versions = array();

        foreach ($return->issues as $issue) {
            if (count($issue->fields->fixVersions) === 0) {
                continue;
            }

            if (!isset($versions[$issue->fields->fixVersions[0]->id])) {
                $versions[$issue->fields->fixVersions[0]->id] = array(
                    'id'          => $issue->fields->fixVersions[0]->id,
                    'description' => $issue->fields->fixVersions[0]->description,
                    'name'        => $issue->fields->fixVersions[0]->name,
                    'issues'      => array(),
                );
            }

            $versions[$issue->fields->fixVersions[0]->id]['issues'][] = array(
                'summary'     => $issue->fields->summary,
                'description' => $issue->fields->customfield_10600,
            );
        }

        return $versions;
    }

    public function getCurrentVersion()
    {
        return trim(file_get_contents(APPPATH . '..' . DIRECTORY_SEPARATOR . 'release' . DIRECTORY_SEPARATOR . getenv('tagfile')));
    }
}