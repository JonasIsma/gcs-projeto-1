<?php

namespace toshyro\gcs\service;

use Mailgun\Mailgun;

class MailgunService
{
    private $to;
    private $from;
    private $subject;
    private $message;
    private $cco;

    protected function __construct()
    {
        $this->from = 'gcs - GED <postmaster@gcs.com.br>';
    }

    /**
     * @return MailgunService
     */
    public static function getInstance()
    {
        return new self;
    }

    /**
     * @param mixed $to
     *
     * @return MailgunService
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param mixed $from
     *
     * @return MailgunService
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param mixed $subject
     *
     * @return MailgunService
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param mixed $message
     *
     * @return MailgunService
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param mixed $cco
     *
     * @return $this
     */
    public function setCco($cco)
    {
        $this->cco = $cco;

        return $this;
    }


    public function send()
    {
        if (ENVIRONMENT === 'development') {
            $this->to  = getenv('email');
            $this->cco = null;
        }

        $mgService = new Mailgun(getenv('mailgun_apikey'));
        $domain    = getenv('mailgun_domain');

        $postData = array(
            'from'    => $this->from,
            'subject' => $this->subject,
            'html'    => $this->message,
        );

        $postData['to'] = array($this->to);
        if ($this->to === null) {
            $postData['to'] = $this->from;
        }

        if ($this->cco !== null) {
            $postData['bcc'] = $this->cco;
        }

        $mgService->sendMessage($domain, $postData);
    }
}