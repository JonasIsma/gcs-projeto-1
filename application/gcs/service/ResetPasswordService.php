<?php

namespace toshyro\gcs\service;
use toshyro\gcs\repository\RepositoryFactory;

/**
 * Created by PhpStorm.
 * User: Jonas Maders
 * Date: 13/02/2019
 * Time: 08:45
 */

class ResetPasswordService
{
    private $usuarioRepository;

    public function __construct()
    {
        $this->usuarioRepository = RepositoryFactory::make('usuarios');
    }

    public function sendResetPasswordTokenTo($usuario)
    {
        $ci = &get_instance();
        $token = $this->generateTokenForPasswordReset($usuario->id);
        $url   = $this->createPasswordResetUrl($token);

        $usuario->url = $url;

        $html = $ci->twig->render('sistema/login/login_esqueceu_senha_email', array('usuario' => $usuario));

        MailgunService::getInstance()
            ->setTo($usuario->email)
            ->setSubject('Recuperar Senha')
            ->setMessage($html)
            ->send();
    }

    private function generateTokenForPasswordReset($usuarioID)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(16));

        $usuario = $this->usuarioRepository->findByID($usuarioID);

        $usuario->tokenacesso = $token;

        $this->usuarioRepository->update(get_object_vars($usuario));

        return $token;
    }

    private function createPasswordResetUrl($token)
    {
        return base_url("sistema/login/resetPassword/{$token}");

    }

    public function resetUserPasswordAndInvalidateToken($usuario, $password)
    {
        $usuario->tokenacesso = null;
        $usuario->senha       = password_hash($password, PASSWORD_DEFAULT);

        $this->usuarioRepository->update(get_object_vars($usuario));
    }
}