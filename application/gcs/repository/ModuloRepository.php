<?php

namespace toshyro\gcs\repository;


class ModuloRepository extends GenericRepository
{
    public function modulosComPermissao($usuario)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('modulo.id, modulo.descricao, modulo.urisegment')
           ->from($this->tableName, 'modulo')
           ->join('modulo', 'permissoes', 'permissao', 'permissao.idmodulo = modulo.id')
           ->join('permissao', 'usuariospermissoes', 'usuariopermissao', 'usuariopermissao.idpermissao = permissao.id')
           ->andWhere('usuariopermissao.idusuario = :usuario')
           ->setParameter('usuario', $usuario);

        return $qb->execute()->fetchAll();
    }
}