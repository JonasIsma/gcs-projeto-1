<?php

namespace toshyro\gcs\repository;


class UsuarioRepository extends GenericRepository
{
    /**
     * @param $data
     *
     * @return integer
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function insert($data)
    {

        if (isset($data['senha']) && $data['senha'] !== $data['confirmasenha']) {
            throw new \Exception('Senhas não conferem.');
        }

        $usuario = array(
            'nome'  => $data['nome'],
            'email' => $data['email'],
            'senha' => isset($data['senha']) ? $data['senha'] : null,
            'ativo' => true,
        );

        if (isset($data['gerarToken']) && $data['gerarToken'] !== '' && isset($data['tokenacesso'])) {
            $usuario['tokenacesso'] = $data['tokenacesso'];
        }

        if (isset($data['senha']) && $data['senha'] !== '') {
            $usuario['senha'] = password_hash($usuario['senha'], PASSWORD_DEFAULT);
        }

        $idUsuario = parent::insert($usuario);

        if (isset($data['permissoes']) && $data['permissoes'] !== "") {
            foreach ($data['permissoes'] as $permissao) {
                RepositoryFactory::make('usuariospermissoes')
                                 ->insert(array(
                                     'idusuario'   => $idUsuario,
                                     'idpermissao' => $permissao,
                                 ));
            }
        }

        if (isset($data['setores']) && $data['setores'] !== "") {
            foreach ($data['setores'] as $setor) {
                RepositoryFactory::make('usuariossetores')
                                 ->insert(array(
                                     'idusuario'   => $idUsuario,
                                     'idsetor' => $setor,
                                 ));
            }
        }

        $novoRegistro = $this->findByID($idUsuario);

        $this->gravarLogInclusao($novoRegistro);

        return $idUsuario;
    }

    public function update($data)
    {
        $this->connection->beginTransaction();

        $usuario = array(
            'id'    => $data['id'],
            'nome'  => $data['nome'],
            'email' => $data['email'],
        );

        $antigo = $this->findByID((int)$usuario['id']);

        if (isset($data['alterarSenha']) && $data['alterarSenha'] === '1') {
            if (isset($data['senha'])) {
                if ($data['senha'] !== $data['confirmasenha']) {
                    throw new \Exception('Senhas não conferem.');
                }

                $usuario['senha']       = password_hash($data['senha'], PASSWORD_DEFAULT);
                $usuario['tokenacesso'] = null;
                $data['tokenacesso'] = null;
            }

            if (isset($data['gerarToken']) && $data['gerarToken'] !== '') {
                $usuario['tokenacesso'] = $this->geraToken();
            }
        }

        if(isset($data['tokenacesso']) && $data['tokenacesso'] != null){
            $usuario['tokenacesso'] = $data['tokenacesso'];
        }

        parent::update($usuario);


        if (isset($data['permissoes']) && $data['permissoes'] !== "") {

            RepositoryFactory::make('usuariospermissoes')
                             ->deleteBy(array(new QueryCriteria('idUsuario', $data['id'])));


            foreach ($data['permissoes'] as $permissao) {
                RepositoryFactory::make('usuariospermissoes')
                                 ->insert(array(
                                     'idUsuario'   => $data['id'],
                                     'idPermissao' => $permissao,
                                 ));
            }
        }

        if (isset($data['permissoes']) && $data['permissoes'] !== "") {

            RepositoryFactory::make('usuariossetores')
                             ->deleteBy(array(new QueryCriteria('idUsuario', $data['id'])));

            if (isset($data['setores']) && $data['setores'] !== "") {
                foreach ($data['setores'] as $setor) {
                    RepositoryFactory::make('usuariosSetores')
                                     ->insert(array(
                                         'idUsuario' => $data['id'],
                                         'idSetor'   => $setor,
                                     ));
                }
            }
        }

        $this->connection->commit();

        $atualizado = $this->findByID($usuario['id']);
        $this->gravarLogAlteracao($antigo, $atualizado);
    }

    public function atualizaPerfil($requestData)
    {
        parent::update($requestData);
    }

    public function getNameByUserId($id){
        $qb = $this->connection->createQueryBuilder();
        $qb->select('usuarios.nome nome')
           ->from($this->tableName, 'usuarios')
           ->where('usuarios.id = :id')
           ->setParameter('id', $id);

        return $qb->execute()->fetchAll();
    }

    private function geraToken()
    {
        return str_pad(rand(1, 999999), 6, rand(1, 9), STR_PAD_RIGHT);
    }

    public function getUsuarioPermissoesByModulo($uriSegment, $valor)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('usuario.id id, usuario.nome nome, usuario.email email, usuario.codigo codigo, usuario.tokenacesso token, usuario.ativo ativo')
           ->from($this->tableName, 'usuario')
           ->join('usuario', 'usuariospermissoes', 'usuarioPermissao', 'usuario.id = usuarioPermissao.idUsuario')
           ->join('usuarioPermissao', 'permissoes', 'permissao', 'permissao.id = usuarioPermissao.idPermissao')
           ->join('permissao', 'modulos', 'modulo', 'modulo.id = permissao.idModulo')
           ->andWhere('modulo.uriSegment = :modulo')
           ->andWhere('permissao.valor = :valor')
           ->andWhere('usuario.ativo = true')
           ->setParameter('modulo', $uriSegment)
           ->setParameter('valor', $valor);

        return $qb->execute()->fetchAll();
    }

    public function ativo($id, $condicao)
    {
        parent::update(array('id' => $id, 'ativo' => $condicao));
    }

    public function importar($data)
    {
        $this->connection->beginTransaction();

        if (isset($data['atualizar'])) {
            foreach ($data['atualizar'] as $codigo => $nome) {
                $usuario = $this->findOneBy(array(
                    new QueryCriteria('codigo', $codigo),
                ));

                if ($usuario === null) {
                    throw new \Exception(sprintf('Usuário %s - %s não encontrado', $codigo, $nome));
                }

                parent::update(array(
                    'id'     => $usuario->id,
                    'nome'   => $nome,
                    'codigo' => $codigo,
                ));
            }
        }

        if (isset($data['novos'])) {
            foreach ($data['novos'] as $codigo => $nome) {
                $this->insert(array(
                    'nome'       => $nome,
                    'codigo'     => $codigo,
                    'email'      => null,
                    'gerarToken' => true,
                    'permissoes' => isset($data['permissoes']) ? $data['permissoes'] : '',
                ));
            }
        }

        $this->connection->commit();
    }

    public function findUsersBySectors($criteria, $orderBy = array()){
        $qb = $this->connection->createQueryBuilder();

        $qb->select('*')
           ->from($this->tableName, 'usuario')
           ->innerJoin('usuario','usuariossetores','usuarioSetor','usuario.id = usuarioSetor.idusuario')
           ->innerJoin('usuarioSetor','setores','setor','usuarioSetor.idsetor = setor.id');

        $this->buildCriteria($qb, $criteria);
        $this->buildOrderBy($qb, $orderBy);

        return $qb->execute()->fetchAll();
    }

    public function isAdministradorGED($idUsuario){
        $permissoes = RepositoryFactory::make('usuariospermissoes')->getPermissoesByUsuario($idUsuario);

        foreach ($permissoes as $permissao) {
            if($permissao->permissaoid == 3)
                return true;
        }

        return false;
    }
}