<?php

namespace toshyro\gcs\repository;

class QueryCriteria
{
    protected $field;
    protected $value;
    protected $operator;

    public function __construct($field, $value, $operator = '=')
    {
        $this->field    = $field;
        $this->value    = $value;
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }
}