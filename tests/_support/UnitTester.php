<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class UnitTester extends \Codeception\Actor
{
//    use _generated\UnitTesterActions;

    /**
     * Define custom actions here
     */

    public function amLoggedIn()
    {
        $_SESSION['dbUser']     = getenv('DBUSER');
        $_SESSION['dbPassword'] = getenv('DBPASSWORD');
        $_SESSION['dbHostName'] = getenv('DBHOSTNAME');
        $_SESSION['dbPort']     = getenv('DBPORT');
        $_SESSION['dbName']     = getenv('UNITDBNAME');
    }
}