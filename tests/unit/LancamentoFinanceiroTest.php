<?php

use toshyro\gcs\repository\IRepository;
use toshyro\gcs\repository\RepositoryFactory;

class LancamentoFinanceiroTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $idLancamentoCreated;

    /** @var IRepository $lancamentoFinanceiroRepository */
    protected $lancamentoFinanceiroRepository;

    protected function _before()
    {
        $this->lancamentoFinanceiroRepository = RepositoryFactory::make('lancamentosfinanceiros');

        $this->idLancamentoCreated = $this->lancamentoFinanceiroRepository->findBy(null, array('id' => 'desc'))[0]->id;
    }

    protected function _after()
    {
    }

    // tests
    public function testLancamentoFinanciroAdd()
    {
        try {
            $lancamentoFinanceiro = $this->getModelLancamentoFinanceiroForAdd();

            $this->idLancamentoCreated = $this->lancamentoFinanceiroRepository->insert($lancamentoFinanceiro);

            $boolean = $this->idLancamentoCreated > 0;

            $this->assertEquals(true, $boolean);

        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testLancamentoFinanceiroVerifyAdd()
    {
        try {
            $lancamentoFinanceiro = get_object_vars($this->lancamentoFinanceiroRepository->findByID($this->idLancamentoCreated));

            $lancamentoFinanceiroDefault = $this->getModelLancamentoFinanceiroForAdd();
            $lancamentoFinanceiroDefault['id'] = $this->idLancamentoCreated;

            $this->validateLancamentoFinanceiroToEquals($lancamentoFinanceiro, $lancamentoFinanceiroDefault);

        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }

    }

    public function testLancamentoFinanceiroEdit()
    {
        try {
            $this->idLancamentoCreated = $this->lancamentoFinanceiroRepository->update($this->getModelLancamentoFinanceiroForEdit());
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testLancamentoFinanceiroVerifyEdit()
    {
        try {

            $lancamentoFinanceiro = get_object_vars($this->lancamentoFinanceiroRepository->findByID($this->idLancamentoCreated));

            $this->validateLancamentoFinanceiroToEquals($lancamentoFinanceiro, $this->getModelLancamentoFinanceiroForEdit());

        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }

    }

    public function testLancamentoFinanceiroDelete(){
        try{
            $this->lancamentoFinanceiroRepository->deleteByID($this->idLancamentoCreated);

            $lancamentoFinanciero = $this->lancamentoFinanceiroRepository->deleteByID($this->idLancamentoCreated);

            $this->assertEquals(null, $lancamentoFinanciero);
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }

    }

    private function getModelLancamentoFinanceiroForAdd()
    {
        $lancamentoFinanceiro = array();

        $lancamentoFinanceiro['descricao']      = "teste automatizado";
        $lancamentoFinanceiro['dataregistro']   = date('Y-m-d') . " 00:00:00";
        $lancamentoFinanceiro['datavencimento'] = date('Y-m-d') . " 00:00:00";
        $lancamentoFinanceiro['status']         = 1;
        $lancamentoFinanceiro['observacao']     = null;
        $lancamentoFinanceiro['valor']          = 200;
        $lancamentoFinanceiro['datapagamento']  = null;
        $lancamentoFinanceiro['tipo']           = 1;

        return $lancamentoFinanceiro;
    }

    private function getModelLancamentoFinanceiroForEdit()
    {
        $lancamentoFinanceiro = array();

        $lancamentoFinanceiro['id']             = $this->idLancamentoCreated;
        $lancamentoFinanceiro['descricao']      = "teste automatizado EDITED";
        $lancamentoFinanceiro['dataregistro']   = date('Y-m-d') . " 00:00:00";
        $lancamentoFinanceiro['datavencimento'] = date('Y-m-d') . " 00:00:00";
        $lancamentoFinanceiro['status']         = 1;
        $lancamentoFinanceiro['observacao']     = null;
        $lancamentoFinanceiro['valor']          = 200;
        $lancamentoFinanceiro['datapagamento']  = null;
        $lancamentoFinanceiro['tipo']           = 1;

        return $lancamentoFinanceiro;
    }

    private function validateLancamentoFinanceiroToEquals($lancamento1, $lancamento2)
    {
        foreach ($lancamento1 as $key => $field) {
            if ($field != $lancamento2[$key]) {
                $this->fail(sprintf('Exception error: field %s = %s is different to field %s = %s ', $key, $field, $key,
                    $lancamento2[$key]));
            }
        }

        return true;
    }
}